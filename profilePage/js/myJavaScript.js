
function getParameter(name){
    const queryString = decodeURIComponent(location.search.replace(/\+/g,"%20"));
    const regex = new RegExp(name+"=([^&]+)");
    const result = regex.exec(queryString);
    if(result)
        return result[1];
    else
        return null;
}


 function init(){
    let trail = document.getElementById("trailimageid");
    trail.style.visibility="hidden";
    let button = document.getElementById("loginButton");
    let gr = document.getElementById("welcomeHeader");
    let name = getCookie("name");
   
    if(name==null){

        firstName = getParameter("firstName");
        lastName = getParameter("lastName");
        if(firstName!=null && lastName!=null){
         name = firstName + " " + lastName;
         setCookie("name",name,1,0,0);
        }
    }
    if(name!=null){
        gr.innerHTML="Hello "+ name;
        button.innerHTML="Afmelden";
    } 
}


function loginAfmelden(){
    let button = document.getElementById("loginButton");
    let gr = document.getElementById("welcomeHeader");
    if(button.innerHTML=="Login"){
        window.location.href="login.html";
    }
    else{
        //er staat nu afmelden, als je daar op klikt moet de naam weggaan en de knop terug op Login komen
        clearCookie("name");
        button.innerHTML="Login";
        gr.innerHTML="Welcome!";

    }
}
var click=false;
var nClicks=0;
var elem;
function playYesSoundAndAddStar(){
    let button = document.getElementById("toggleAnimation");
    if(button.innerHTML=="Stop Animations"){
        var audio = new Audio("wav/yes.wav");
        audio.play();
        nClicks++;
        const layer = document.getElementById("ok-icon");
        const av = document.getElementById("avatar");
        var rectAv = av.getBoundingClientRect();
        var centerAvX = rectAv.left+rectAv.width*0.5;
        var centerAvY = rectAv.top+rectAv.height*0.5;
    

        layer.style.left = centerAvX-25+"px";
        layer.style.top= centerAvY-25+"px";
        if(nClicks==1){

            elem = document.createElement("img");
            elem.src="img/ok-icon.png";
            elem.style.width="50px";
            elem.style.height="50px";
            elem.style.border="none";
            layer.appendChild(elem);
        }
        click=!click;
        if(click){
            layer.style.visibility="visible";
        }
        else{
            layer.style.visibility="hidden";
        }
    }    
}
var xSanta;
var timer;
const speed = 5;
function startSantaAnimation(){
    const santaImg = document.getElementById("santa");
    santaImg.style.visibility="visible";
    //xSanta = santaImg.getBoundingClientRect().left;
    xSanta = -50;
    timer = setInterval(santaAnimation,20);
}
function stopSantaAnimation(){
    const santaImg = document.getElementById("santa");
    santaImg.style.visibility="hidden";
    clearInterval(timer);
}

function santaAnimation(){
    const santaImg = document.getElementById("santa");
  
    xSanta+=speed;
    if(xSanta>screen.width){
        stopSantaAnimation();
    }
    santaImg.style.left=xSanta+"px";

}
var timer2;
function toggleAnimations(){
    let button = document.getElementById("toggleAnimation");
    let p = document.getElementById("pul");
    let trail = document.getElementById("trailimageid");
    if(button.innerHTML=="Stop Animations"){
        //we must stop all animations
        //hide the ok-button
        document.getElementById("ok-icon").style.visibility="hidden";
        //hide the pulse text
        p.style.visibility="hidden";
        //hide the trail
        trail.style.visibility="hidden";
        //stop santa
        const santaImg = document.getElementById("santa");
        santaImg.style.visibility="hidden";
        clearInterval(timer);
        clearInterval(timer2);
        //
        
        //we change the button to "Start Animations"
        button.innerHTML="Start Animations"
    }
    else{
        //we must start all animations
        timer2 = setInterval(startSantaAnimation,10000); 
        //make pulse visible
        p.style.visibility="visible";
        //make trail visible
        trail.style.visibility="visible";


        //we change the button to "Stop Animations"
        button.innerHTML="Stop Animations";

    }
    

}

window.addEventListener("load",init);