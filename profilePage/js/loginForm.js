function validate(e){
    let valid = true; //only true when all fields are valid
    let validFirstName=true;
    let validLastName=true;
    let validMail =true;
    const firstNameField = document.getElementById("firstname");
    const lastNameField = document.getElementById("lastname");
    const mailField=document.getElementById("mail");
    let mailregex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

    if(firstNameField.value.length<3){
        valid = false;
        validFirstName=false;
        firstNameField.setAttribute("class","error");
    } else{
        firstNameField.removeAttribute("class");
    }

    if(lastNameField.value.length==0){
        valid = false;
        validLastName=false;
        lastNameField.setAttribute("class","error");
    } else{
        lastNameField.removeAttribute("class");
    }

    if(!mailregex.test(mailField.value)){
       valid = false;
       validMail=false;
       mailField.setAttribute("class","error");
    }
    else{
        mailField.removeAttribute("class");
    }

    if(!validFirstName){
        const errorElement=document.getElementById("firstNameError");
        errorElement.innerHTML="First name must be at least 3 characters. Please try again.";
    }

    if(!validLastName){
        const errorElement=document.getElementById("lastNameError");
        errorElement.innerHTML="Lastname cannot be empty. Please try again.";
    }
    
    if(!validMail){
       const errorElement=document.getElementById("mailError");
       errorElement.innerHTML="Unvalid email adress. Please try again.";
    }

    if(valid){
        saveForm(this);
    }
    else{
        e.preventDefault();
    }
}

function reset(e){
    document.getElementById("firstNameError").innerHTML="";
    document.getElementById("lastNameError").innerHTML="";
    document.getElementById("mailError").innerHTML="";
    document.getElementById("firstname").removeAttribute("class");
    document.getElementById("lastname").removeAttribute("class");
    document.getElementById("mail").removeAttribute("class");
}

function init(){
    const form = document.getElementById("myLoginForm");
    loadForm(form);
    form.addEventListener("submit",validate);
    form.addEventListener("reset",reset);
}
window.addEventListener("load",init);

